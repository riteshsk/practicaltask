import React, { useEffect, useState, } from "react";
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useSelector, useDispatch } from "react-redux";
import SignInScreen from '../screens/auth/LoginPhone';
import VerificationCode from '../screens/auth/VerificationCode';
import HomeScreen from '../screens/home/HomeScreen'

const Stack = createNativeStackNavigator();

function Route(props) {

  const userInfo = useSelector((state) => state.userInfo);

  return (
    <NavigationContainer>
       {userInfo.auth ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Home" component={HomeScreen} />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator initialRouteName="Login" screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Login" component={SignInScreen} />
          <Stack.Screen name="Verification" component={VerificationCode} />

        </Stack.Navigator>
      )}

    </NavigationContainer>
  );
}

export default Route;