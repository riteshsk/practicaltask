import React, {useRef, useState} from 'react';
import {TextInput} from 'react-native';

const InputField = (props) => {
  const [focused, setFocused] = useState(false);
  return (
    <>
      <TextInput
        // value={props.textname}
        onChangeText={(value) => props.changeText(value)}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderColor}
        keyboardType={props.texttype}
        defaultValue={props.defaultVal}
        secureTextEntry={props.secureText}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
        style={{
          height: 45,
          marginLeft: 16,
          color:"#000",
          flex: 1,
        }}
      />
    </>
  );
};

export default InputField;
