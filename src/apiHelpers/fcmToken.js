import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from "react-redux";


export const getFcmToken = async () => {
    let fcmToken = await AsyncStorage.getItem('fcmToken')
    
    console.log("the user old token===>", fcmToken)


    if (!fcmToken) {
        try {
            const fcmToken = await messaging().getToken();
            if (fcmToken) {
                console.log("new generated fcm token1111===>", fcmToken),
                await AsyncStorage.setItem('fcmToken', fcmToken)
            }
        } catch (error) {
            console.log("error rasied fcm token===>", error)
            showError(error.message)
        }
    }
}
