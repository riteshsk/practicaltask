import React from 'react';
import axios from "axios";
// import {API_URL} from "@env";
let userToken = "";
const BASE_URL = "https://trootechproducts.com:8020/api/user/"
////////////////// Phone Number login with OTP.............
export const loginPhone = async (code, number) => {
  var axios = require('axios');
  var data = JSON.stringify({
    "contact_number": number,
    "country_code": code
  });

  var config = {
    method: 'post',
    url: `${BASE_URL}generate-otp`,
    headers: {
      'Content-Type': 'application/json'
    },
    data: data
  };

  let result = axios(config)
    .then(function (response) {
      return JSON.stringify(response.data.data);
    })
    .catch(function (error) {
      return error;
    });
  return result;
};


export const login = async (otp, country_code, contact_number, device_type, fcmTokenData) => {
  console.log("all>>>>",otp, country_code, contact_number, device_type, fcmTokenData);
  var axios = require('axios');
  var data = JSON.stringify({
    contact_number: contact_number,
    country_code: country_code,
    otp: otp,
    device_type: device_type,
    device_token: fcmTokenData
  });

  var config = {
    method: 'post',
    url: `${BASE_URL}login`,
    headers: {
      'Content-Type': 'application/json',
       },
    data: data
  };

  let result = axios(config)
    .then(function (response) {
      return JSON.stringify(response.data);
    })
    .catch(function (error) {
      return error;
    });
  return result;
};

