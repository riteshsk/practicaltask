import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Dimensions,
    Alert,
    ScrollView, ImageBackground, SafeAreaView
} from 'react-native';
import { useNavigation } from "@react-navigation/native";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { useSelector, useDispatch } from "react-redux";
import { CheckConnection } from '../../utils/functions';
import { Icon } from 'react-native-elements';

const HomeScreen = (props) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const userInfo = useSelector((state) => state.userInfo.userInfo);
    // console.log("user data:::===>", userInfo);

    let network = CheckConnection();
    if (network === false) {
        Alert.alert(
            "No Internet",
            "Please ensure you are connected to the Internet.",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
        );
        return false;
    }

    const data = [
        {
            name: "data1",
            subdata: [

                { id: 1, subname: "Comunity", image: "https://img.icons8.com/clouds/100/000000/groups.png", },
                { id: 2, subname: "Housing", image: "https://img.icons8.com/color/100/000000/real-estate.png", },
                { id: 3, subname: "Jobs", image: "https://img.icons8.com/color/100/000000/find-matching-job.png", },

            ]
        },
        {
            name: "data2",
            subdata: [
                { id: 4, subname: "Personal", image: "https://img.icons8.com/clouds/100/000000/employee-card.png", },
                { id: 4, subname: "Personal", image: "https://img.icons8.com/clouds/100/000000/employee-card.png", },
                { id: 4, subname: "Personal", image: "https://img.icons8.com/clouds/100/000000/employee-card.png", },
            ]
        },
        {
            name: "data3",
            subdata: [
                { id: 1, subname: "Comunity", image: "https://img.icons8.com/clouds/100/000000/groups.png", },
                { id: 2, subname: "Housing", image: "https://img.icons8.com/color/100/000000/real-estate.png", },
                { id: 3, subname: "Jobs", image: "https://img.icons8.com/color/100/000000/find-matching-job.png", },
            ]
        },
    ]

    const [open, setOpen] = useState(false)

    const image = { uri: "https://reactjs.org/logo-og.png" };

    const onClickListener = async () => {
        dispatch({
            type: "SET_AUTH",
            payload: false,
        });

    }
    return (
        <SafeAreaProvider>
            <View style={styles.container}>
                <View style={styles.backView}>
                    <ImageBackground source={image} resizeMode="cover" style={{ flex: 1 }}>
                        <View style={{ flexDirection: "row", }}>
                            <View style={styles.iconView}>
                                <Icon
                                    name='menu'
                                    type='ionicons'
                                    color='#fff'
                                    size={30}
                                    style={styles.hedIcon}
                                />
                            </View>
                            <View style={{ width: "60%" }}></View>
                            <TouchableOpacity style={styles.iconView} onPress={() => onClickListener()}>
                                <Icon
                                    name='log-out'
                                    type='entypo'
                                    color='#fff'
                                    size={30}
                                    style={styles.hedIcon}
                                    onPress
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ padding: 15 }}>
                            <Text style={styles.hedText}>Welcome,</Text>
                            <Text style={styles.hedTextDec}>Jhone Doe,</Text>
                        </View>

                    </ImageBackground>
                </View>

                <View style={styles.secView}>

                    <View style={styles.imgView}>
                        <Image style={styles.img} source={{ uri: "https://img.icons8.com/clouds/100/000000/employee-card.png" }} />
                    </View>

                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.comText}>ABC Company Ltd</Text>
                        <Text style={styles.comTextDec}>The Hearst Tower</Text>
                    </View>
                </View>

                <ScrollView>
                    <View style={styles.cardView}>
                        {data.map((item, i) => {
                            return (

                                <View style={styles.card}>
                                    <View style={{ flexDirection: "row" }}>
                                        <View style={styles.cardUser}>
                                            <View style={styles.cardImgView} >
                                                <Image style={styles.userImg} source={{ uri: item.image }} />
                                            </View>
                                        </View>
                                        <View style={styles.userConView}>
                                            <Text style={styles.userText}>{item.name}</Text>
                                            <Text style={styles.userTextDec}>3 Pending Requests</Text>
                                        </View>
                                        <View style={styles.cont}>
                                            <TouchableOpacity onPress={() => setOpen(item.name)}>
                                                <Icon
                                                    name='chevron-down'
                                                    type='entypo'
                                                    color='#000'
                                                    size={30}
                                                    style={styles.arroIcon}
                                                />
                                            </TouchableOpacity>

                                        </View>

                                    </View>
                                    {open == item.name &&
                                        <View>
                                            {item.subdata.map((item2, i) => {
                                                return (
                                                    <View style={styles.subCard}>
                                                        <View style={{ flexDirection: "row" }}>
                                                            <View style={styles.cardUser}>
                                                                <View style={styles.cardImgView} >
                                                                    <Image style={styles.userImg} source={{ uri: item2.image }} />
                                                                </View>
                                                            </View>
                                                            <View style={styles.userConView}>
                                                                <Text style={styles.userText}>Guest Requests</Text>
                                                                <Text style={styles.userTextDec}>3 Pending Requests</Text>
                                                            </View>

                                                        </View>
                                                    </View>
                                                );
                                            })}
                                        </View>
                                    }
                                </View>
                            );
                        })}
                    </View>
                </ScrollView>
            </View>
        </SafeAreaProvider>
    );

}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
    },
    iconView:{
        width: "20%", 
        justifyContent: "center"
    },
    backView: {
        height: "35%",
        width: "100%",
    },
    hedIcon: {
        alignItems: "flex-start",
        padding: 15
    },
    hedText: {
        fontSize: 28,
        fontWeight: "bold"
    },
    hedTextDec: {
        fontSize: 16,
        fontWeight: "bold"
    },
    secView: {
        height: "18%",
        width: "100%",
        backgroundColor: "#fafafa",
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        zIndex: 999,
        marginTop: -30
    },
    imgView: {
        width: 100,
        height: 100,
        backgroundColor: "#fff",
        alignSelf: "center",
        zIndex: 999,
        marginTop: -50,
        borderRadius: 50,
        elevation: 9
    },
    img: {
        width: 100,
        height: 100,
    },
    comText: {
        color: "#000",
        textAlign: "center",
        fontSize: 20,
        fontWeight: "bold"
    },
    comTextDec: {
        color: "#b4b4b4",
        textAlign: "center",
        fontSize: 14,
        fontWeight: "bold"
    },
    cardView: {
        flexDirection: "column",
        width: "100%",
    },
    card: {
        shadowColor: '#00000021',
        shadowOffset: {
            width: 0,
            // height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 2.49,
        elevation: 3,
        alignSelf: "center",
        width: "90%",
        marginTop: 10,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 10,
    },
    subCard: {
        shadowColor: '#00000021',
        shadowOffset: {
            width: 0,
            // height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 2.49,
        elevation: 3,
        alignSelf: "center",
        width: "100%",
        marginTop: 10,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 10,
    },
    cardUser: {
        backgroundColor: "#fff",
        width: "20%",
        height: 50,
        alignSelf: "center",
        justifyContent: "center"
    },
    cardImgView: {
        width: 50,
        height: 50,
        borderRadius: 60,
        backgroundColor: "#dcdede"
    },
    userImg: {
        borderWidth: 1,
        width: 50,
        height: 50,
        borderRadius: 60,
        alignSelf: "center"
    },
    userConView: {
        alignSelf: "center",
        justifyContent: "center",
        width: "60%"
    },
    userText: {
        fontSize: 14,
        color: "#000",
        fontWeight: "700",
        marginLeft: 20
    },
    userTextDec: {
        fontSize: 10,
        color: "#929494",
        fontWeight: "700",
        marginLeft: 20
    },
    cont: {
        width: "20%",
        justifyContent: "center"
    },
    arroIcon: {
        alignItems: "flex-start",
        padding: 15
    }
}); 