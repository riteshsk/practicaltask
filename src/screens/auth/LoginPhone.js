import React, { useEffect, useState, } from "react";
import {
    StyleSheet,
    Text,
    View,
    Platform,
    TouchableOpacity, Alert
} from 'react-native';
import { useNavigation } from "@react-navigation/native";
import InputField from '../../Components/InputField';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { loginPhone } from '../../apiHelpers/Api'
import { CheckConnection } from '../../utils/functions';

const LoginPhone = (props) => {
    const navigation = useNavigation();

    const [phoneNumber, setPhoneNumber] = useState('');
    const [code, setCode] = useState('+91');
    const [deviceName, setDeviceName] = useState('');

    const phoneno = /^\d{10}$/;

    let network = CheckConnection(); //////////Checking Internet.....
    if (network === false) {
        Alert.alert(
            "No Internet",
            "Please ensure you are connected to the Internet.",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
        );
        return false;
    }

    useEffect(()  => {
        if (Platform.OS === 'ios') {
            setDeviceName("ios")
        }
        else {
            setDeviceName("android")
        }
    }, [])

    const onClickListener = async () => {

        if (!phoneno.test(phoneNumber)) {
            Alert.alert("Invalid Phone number")
            return false;
        }
        else {
            let response = await loginPhone(code, phoneNumber); //////this is a API Call....
            const obj = JSON.parse(response);

            const data = {};

            data['contact_number'] = obj.contact_number;
            data['country_code'] = obj.country_code;
            data['otp'] = obj.otp;
            data['device_type'] = deviceName;
            navigation.navigate("Verification", { item: data });
        }


    }

    return (
        <SafeAreaProvider>
            <View style={styles.container}>
                <View style={styles.headView}>
                    <Text style={styles.text}>Enter Mobile Number</Text>
                </View>

                <View style={styles.inputContainer}>
                    <Text style={styles.contCode}>+91</Text>
                    <InputField
                        textname={phoneNumber}
                        texttype="numeric"
                        changeText={(value) => setPhoneNumber(value)}
                        placeholder="Enter Mobile Number"
                        placeholderColor="#000"
                    />
                </View>


                <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => onClickListener()}>
                    <Text style={styles.loginText}>Submit</Text>
                </TouchableOpacity>

            </View>
        </SafeAreaProvider>
    );

}
export default LoginPhone;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        borderBottomWidth: 1,
        width: "70%",
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: "50%",
        borderRadius: 10,
    },
    loginButton: {
        backgroundColor: "#00b5ec",
    },
    loginText: {
        color: 'white',
    },
    headView: {
        marginTop: 10,
        marginBottom: 20
    },
    text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#000"
    },
    contCode: {
        fontWeight: "bold",
        marginLeft: 10,
        color: "#000",
    }

});