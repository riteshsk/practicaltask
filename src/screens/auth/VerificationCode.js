import React, { useEffect, useState, } from "react";
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import { useNavigation } from "@react-navigation/native";
import messaging from '@react-native-firebase/messaging';
import { useSelector, useDispatch } from "react-redux";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import InputField from '../../Components/InputField';
import { CheckConnection } from '../../utils/functions';
import { login } from '../../apiHelpers/Api'


const VerificationCode = (props) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    /////// Get login data............ 
    const user = props.route.params.item
    const otp = user.otp
    const country_code = user.country_code
    const contact_number = user.contact_number
    const device_type = user.device_type
    const fcmTokenData = useSelector((state) => state.userInfo.fcmToken);

    useEffect(() => {
        getFcmToken();
    }, []);

    const getFcmToken = async () => {
        const fcmToken = await messaging().getToken();
        dispatch({
            type: "SET_FCM",
            payload: fcmToken,
        });
    }

    let network = CheckConnection();//////////Checking Internet.....
    if (network === false) {
        Alert.alert(
            "No Internet",
            "Please ensure you are connected to the Internet.",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
        );
        return false;
    }


    const onClickListener = async () => {
        let response = await login(otp,country_code,contact_number,device_type,fcmTokenData); //////this is a API Call....
        const obj = JSON.parse(response);
        dispatch({
            type: "SET_USER",
            payload: obj,
        });
        
        dispatch({
            type: "SET_AUTH",
            payload: true,
        });
        navigation.navigate("Home");
    }

    return (
        <SafeAreaProvider>
            <View style={styles.container}>
                <View style={styles.headView}>
                    <Text style={styles.text}>Verify OTP </Text>
                </View>

                <View style={styles.inputContainer}>
                    <Text style={{ marginLeft: 20, color: "#000" }}>{user.otp}</Text>
                </View>

                <View style={styles.inputContainer}>
                    <Text style={{ marginLeft: 20, color: "#000" }}>{user.country_code} {user.contact_number}</Text>
                </View>

                <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => onClickListener()}>
                    <Text style={styles.loginText}>Login</Text>
                </TouchableOpacity>

            </View>
        </SafeAreaProvider>
    );

}
export default VerificationCode;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        borderBottomWidth: 1,
        width: "70%",
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: "50%",
        borderRadius: 10,
    },
    loginButton: {
        backgroundColor: "#00b5ec",
    },
    loginText: {
        color: 'white',
    },
    headView: {
        marginTop: 10,
        marginBottom: 20
    },
    text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#000"
    }
});